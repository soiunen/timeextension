/*
script that deals with break time logic
 */

//starts the break timer and loads image
document.body.onload = function () {
    setTimer(parseInt(getUrlParam('time', 5)));
    getImage();
};

//next image
document.getElementById("next").onclick = function () {
    getImage();
};

//function to request new random image
function getImage() {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `https://dog.ceo/api/breeds/image/random`, true);
    xhr.onload = function () {
        if (this.status === 200) {
            const response = JSON.parse(this.responseText);
            if (response.status === 'success') {
                document.getElementById("picture").src = response.message;
            } else {
                document.getElementById('card').innerText = 'Something went wrong';
            }
        }
    };
    xhr.send();
}


function getUrlVars() {
    let vars = {};
    let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

//function to get timer parameter
function getUrlParam(parameter, defaultValue) {
    let urlParameter = defaultValue;
    if (window.location.href.indexOf(parameter) > -1) {
        urlParameter = getUrlVars()[parameter];
    }
    return urlParameter;
}

//timer logic
function setTimer(min) {
    let countDownDate = new Date();
    countDownDate.setMinutes(countDownDate.getMinutes() + min);

    let x = setInterval(function () {

        let now = new Date().getTime();
        let distance = countDownDate - now;

        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("timer").innerHTML = `<p class="text-white" style="font-size: medium;">Break time!</p>${minutes} : ${seconds}`;
        if (distance < 0) {
            window.close();
        }
    }, 1000);
}