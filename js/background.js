/*
*script that runs on installation
*/

//sets default values for time tracker
chrome.runtime.onInstalled.addListener(function () {
    chrome.storage.sync.set({
        "workInterval": 25,
        "smallBreakInterval": 5,
        "largeBreakInterval": 15,
        "nrOfSmallIntervals": 4,
        "apiToken": ''
    }, function () {
        if (chrome.runtime.error) {
            console.log("Runtime error.");
        }
    });
});