/*
* script that deals with options, like small break time, working time, large break time,
* number of intervals and API token
*/

//gets options from storage
document.body.onload = function () {
    chrome.storage.sync.get([
        "workInterval",
        "smallBreakInterval",
        "largeBreakInterval",
        "nrOfSmallIntervals",
        "apiToken"], function (result) {
        if (!chrome.runtime.error) {
            console.log(result);
            document.getElementById('workInterval').value = result.workInterval;
            document.getElementById('smallBreakInterval').value = result.smallBreakInterval;
            document.getElementById('largeBreakInterval').value = result.largeBreakInterval;
            document.getElementById('nrOfSmallIntervals').value = result.nrOfSmallIntervals;
            document.getElementById('apiToken').value = result.apiToken;
        }
    });
};

//saves options to storage
document.getElementById("saveOptions").onclick = function () {
    if (document.getElementById('apiToken').value === '') {
        alert("To track time in Toggle, please enter API token that can be found under user settings at toggl.com");
    } else {
        chrome.storage.sync.set({
            "workInterval": document.getElementById('workInterval').value,
            "smallBreakInterval": document.getElementById('smallBreakInterval').value,
            "largeBreakInterval": document.getElementById('largeBreakInterval').value,
            "nrOfSmallIntervals": document.getElementById('nrOfSmallIntervals').value,
            "apiToken": document.getElementById('apiToken').value
        }, function () {
            if (chrome.runtime.error) {
                console.log("Runtime error.");
            }
        });

        document.getElementById('message').innerText = 'Options saved';
        setTimeout(() => {
            document.getElementById('message').innerText = '';
        }, 2000);
    }
};