/*
 script that deals with popup buttons
 */

document.addEventListener('DOMContentLoaded', pageActions, false);

function pageActions() {
    document.getElementById('optionsButton').addEventListener('click', () => {
        chrome.tabs.create({'url': "/options.html"});
    });

    document.getElementById('start').addEventListener('click', () => {
        chrome.tabs.create({'url': "/pomodoro.html"});
    });
}


