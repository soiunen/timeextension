/*
script that holds the timer logic
 */
let vars = {
    task: '',
    cycle: 1,
    workInterval: 0,
    smallBreakInterval: 0,
    largeBreakInterval: 0,
    nrOfSmallIntervals: 0,
    apiToken: ''
};
chrome.storage.sync.get([
    "workInterval",
    "smallBreakInterval",
    "largeBreakInterval",
    "nrOfSmallIntervals",
    "apiToken"], function (result) {
    if (!chrome.runtime.error) {
        vars.workInterval = parseInt(result.workInterval);
        vars.smallBreakInterval = parseInt(result.smallBreakInterval);
        vars.largeBreakInterval = parseInt(result.largeBreakInterval);
        vars.nrOfSmallIntervals = parseInt(result.nrOfSmallIntervals);
        vars.apiToken = result.apiToken;
    }
});

document.getElementById('work').addEventListener('click', () => {
    let task = document.getElementById('task').value;
    if (task === "") {
        document.getElementById("timer").innerHTML = `<p class="text-danger" style="font-size: medium;">Please enter task</p>`;
    } else {
        vars.task = task;
        setTimer(vars.workInterval);
    }

});

function setTimer(min) {
    let countDownDate = new Date();
    countDownDate.setMinutes(countDownDate.getMinutes() + min);

    let x = setInterval(function () {

        let now = new Date().getTime();
        let distance = countDownDate - now;

        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("timer").innerHTML = `<p class="text-white" style="font-size: medium;">${vars.task}</p>${minutes} : ${seconds}`;

        document.getElementById('stop').addEventListener('click', () => {
            clearInterval(x);
            vars.cycle = 1;
            document.getElementById("timer").innerHTML = `${vars.workInterval} : 00`;
            document.getElementById("message").innerHTML = ``;
        });
        if (distance < 0) {
            vars.cycle += 1;
            if (vars.cycle === vars.nrOfSmallIntervals * 2) {
                clearInterval(x);
                console.log("cycle is " + vars.cycle);
                vars.cycle = 0;
                console.log("cycle is " + vars.cycle);
                document.getElementById("message").innerHTML =
                    '<p class="text-white" style="font-size: medium;">Break time!</p>';
                setTimer(vars.largeBreakInterval);
                chrome.tabs.create({'url': `/pictures.html?time=${vars.largeBreakInterval}`});
            } else if (vars.cycle % 2 !== 0) {
                clearInterval(x);
                setTimer(vars.workInterval);
                console.log("cycle is " + vars.cycle);
            } else if (vars.cycle % 2 === 0) {
                clearInterval(x);
                document.getElementById("message").innerHTML =
                    '<p class="text-white" style="font-size: medium;">Break time!</p>';
                setTimer(vars.smallBreakInterval);
                console.log("cycle is " + vars.cycle);
                chrome.tabs.create({'url': `/pictures.html?time=${vars.smallBreakInterval}`});
            }
        }
    }, 1000);
}


//function to test response from toggl API that cannot get response because toggl
// seems have blocked requests and require CORS whitelisting on toggl end
function req() {
    const token = "Basic " + btoa(vars.apiToken + ":api_token");
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `https://www.toggl.com/api/v8/me`, false);
    xhr.setRequestHeader("Authorization", token);
    xhr.setRequestHeader("Content-Type", "application/json");
    console.log(token);
    console.log(this.responseText);
    xhr.send();

}