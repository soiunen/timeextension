# timeExtension
Author: Timo Soiunen
Year: 2019

This is a chrome extension that uses pomodoro method to track time.
After installation one can either start tracking time or modify default values.
On break times it uses API that displays random dog pictures.

It was supposed to have integration with Toggl time tracking, but as their API 
requires CORS whitelisting it is not possible to send requests at this point.
Function was added to test Toggl authentication.
